package it.bertolottipf.poesie.utility;

import it.bertolottipf.poesie.PoesieBPF;
import it.bertolottipf.poesie.Suoni;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JList;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.Namespace;
import org.jdom.input.SAXBuilder;

/*
 * Uso questa classe per...
 * mettere icone nella lista
 * mettere pulsante play e apri immagine
 */
public class LoadXMLSuoni {

	public static void parseStream(String tipoRicerca) {
		parseStream(null, tipoRicerca);
	}

	/**
	 * Dato il titolo di una poesia, se il titolo poesia � presente nell'xml
	 * LoadXMLSuoni.xml ...
	 * 
	 * @param titoloPoesia
	 * @param tipoRicerca
	 */
	public static void parseStream(String titoloPoesia, String tipoRicerca) {

		Document document = null;

		String nomeFile = new String("resources/xmls/databaseSuoni.xml");

		SAXBuilder saxBuilder = new SAXBuilder();

		// apertura stream per lettura dal file xml ricevuto in input
		try {

			document = saxBuilder.build(new File(nomeFile));

		} catch (JDOMException ex) {

			System.err.println(ex);

		} catch (IOException ex) {

			System.err.println(ex);

		}

		// ricerca dell'elemento radice
		Element rootElement = document.getRootElement();

		Namespace nameSpace = rootElement.getNamespace();

		// lista dei figli dell'elemento radice
		List suoniList = rootElement.getChildren("suono", nameSpace);

		if (tipoRicerca == "Suona") {
			if (suoniList != null && suoniList.size() > 0) {

				for (int i = 0; i < suoniList.size(); i++) {

					Element getFigliElement = (Element) suoniList.get(i);

					String poesia = getFigliElement.getChildText("poesia", nameSpace);
					String file = getFigliElement.getChildText("file", nameSpace);
					if (poesia.equals(titoloPoesia)) {
						Suoni mysound = new Suoni(file);
						break;
					}

					// String testo =
					// getFigliElement.getChildText("testo",nameSpace);

				}
			}
		} else if (tipoRicerca == "AggiungiIconeAllaSelezione") {
			if (suoniList != null && suoniList.size() > 0) {

				for (int i = 0; i < suoniList.size(); i++) {

					Element getFigliElement = (Element) suoniList.get(i);

					String poesia = getFigliElement.getChildText("poesia", nameSpace);
					String file = getFigliElement.getChildText("file", nameSpace);
					if (poesia.equals(titoloPoesia)) {
						Icon icon;
						icon = new ImageIcon("resources/imgs/suono.png");

						PoesieBPF.tit.setIcon(icon);
						break;
					} else {
						Icon icon;
						icon = new ImageIcon();
						PoesieBPF.tit.setIcon(icon);
					}

				}

			}
		}
	}

}