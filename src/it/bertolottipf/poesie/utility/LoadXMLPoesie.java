package it.bertolottipf.poesie.utility;

import it.bertolottipf.poesie.PoesieBPF;

import java.io.File;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.util.List;

import javax.swing.JList;

import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.Namespace;
import org.jdom.input.SAXBuilder;


public class LoadXMLPoesie {
	
	/**
	 * Ricerca di tutti i titoli (RTIT) 
	 * dal file xml nomefile
	 * 
	 * @param tipoRicerca
	 */
	public static void parseStream(String tipoRicerca) {
		parseStream(tipoRicerca, null);
	}
	
	
	/**
	 * Ricerca dei testi per titolo (RTPT) 
	 * dal file xml nomefile
	 * 
	 * @param tipoRicerca
	 * @param titolo
	 */
	public static void parseStream(String tipoRicerca, String titolo) {
		
		org.jdom.Document document = null;
		
		String nomeFile = new String("resources/xmls/databasePoesie.xml");
		
		
		SAXBuilder saxBuilder =	new SAXBuilder();

		//apertura stream per lettura dal file xml ricevuto in input
		try	{
			
			document = saxBuilder.build(new File(nomeFile));
			//document = saxBuilder.build(LoadXMLPoesie.class.getResource("/xmls/databasePoesie.xml"));
			
		} catch (JDOMException ex) {
			
			System.err.println(ex);
			
		} catch (IOException ex) {
			
			System.err.println(ex);
		}

		
		//ricerca dell'elemento radice
		Element rootElement = document.getRootElement();

		Namespace nameSpace = rootElement.getNamespace();

		//lista dei figli dell'elemento radice
		List poesiaList = rootElement.getChildren("poesia", nameSpace);
		
		
		// Tipo ricerca RTIT (Ricerca Tutti I Titoli)
		if (tipoRicerca == "RTIT") {
			if (poesiaList != null && poesiaList.size() > 0) {
				
				for (int i = 0; i < poesiaList.size(); i++) {
	
					Element getFigliElement = (Element) poesiaList.get(i);
		
					String titoloSel = getFigliElement.getChildText("titolo",nameSpace);
					PoesieBPF.model.addElement(titoloSel);
					
					//String testo = getFigliElement.getChildText("testo",nameSpace);
					
				}
			}
			PoesieBPF.listaPoesie = new JList(PoesieBPF.model);
			PoesieBPF.listaPoesie.setOpaque(false);
		}
		
		// Tipo ricerca RTPT (Ricerca Testo Per Titolo)
		if (tipoRicerca == "RTPT") {
			String titoloSel;
			String testoSel;
			
			for (int i = 0; i < poesiaList.size(); i++) {

				Element getFigliElement = (Element) poesiaList.get(i);
	
				titoloSel = getFigliElement.getChildText("titolo",nameSpace);
				testoSel = getFigliElement.getChildText("testo",nameSpace);
				
				if (titoloSel.equals(titolo)) {
					
					PoesieBPF.tit.setText(titoloSel);
					//System.out.println(titoloSel);
					//System.out.println(titolo);
					
					LoadXMLSuoni.parseStream(titoloSel, "AggiungiIconeAllaSelezione");
					LoadXMLSuoni.parseStream(titoloSel, "Suona");
					
					
					PoesieBPF.testoPoesia.setText(testoSel.toString());
					//System.out.println(testoSel);
					
					break;
				}
				
			}
			
		}
	}
	
}