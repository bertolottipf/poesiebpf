package it.bertolottipf.poesie;


import java.io.File;
import java.io.IOException;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;


public class Suoni {
	
	static Clip ol;
	
	/**
	 * Preso un file (persorco + nome file) fa suonare il file
	 * 
	 * @param file
	 */
	public Suoni(String file) {
		
		
		
		File sf = new File(file);
		AudioFileFormat aff;
		AudioInputStream ais;
		try
		{
			aff=AudioSystem.getAudioFileFormat(sf);
			ais=AudioSystem.getAudioInputStream(sf);
			AudioFormat af=aff.getFormat();
			DataLine.Info info = new DataLine.Info(
					Clip.class,
					ais.getFormat(),
					((int) ais.getFrameLength() * af.getFrameSize()));
			ol = (Clip) AudioSystem.getLine(info);
			
			
	        ol.open(ais);

	        
	        
	        
	        
			
			//ol.loop(Clip.LOOP_CONTINUOUSLY);
			ol.loop(0);
			//System.out.println("Riproduzione iniziata, premere CtrL-C per interrompere");
		} catch(UnsupportedAudioFileException ee){
			//System.out.println(ee);
		} catch(IOException ea){
			//System.out.println(ea);
		} catch(LineUnavailableException LUE){
			//System.out.println(LUE);
		} catch (Exception e) {
			//System.out.println(e);
		}
		
	}
}
