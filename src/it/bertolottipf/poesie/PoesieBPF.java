package it.bertolottipf.poesie;


import it.bertolottipf.poesie.utility.LoadXMLPoesie;

import java.io.FileNotFoundException;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.jdom.JDOMException;



public class PoesieBPF {
	
	private JPopupMenu popup;
	//private Action cutAction;
	private Action copyAction;
	//private Action pasteAction;

	private JPanel jpTitle;
	private JPanel jpTitles;
	private JPanel jpContents;
	//private JPanel jpStart;
	private Font serif20Bold = new Font("Serif", Font.BOLD, 26);
	private JFrame frame;
	private static JScrollPane scrollbar;
	
	public static JLabel tit;
	public static JTextArea testoPoesia;
	public static DefaultListModel model;
	public static JList listaPoesie;
	
	
	public PoesieBPF() throws Throwable, JDOMException{
		
		///////////////////////////////////
		//   Creo e setto la finestra.   //
		///////////////////////////////////
		
		/**
		 * Setto i valori largezza e altezza iniziali della
		 * finestra
		 * 
		 * frameWidth > la larghezza del frame
		 * frameHeight > l'altezza del frame
		 */
		int frameWidth = 800;
		int frameHeight = 561;
		
		
		/**
		 * Setto titolo, altezza e larghezza della finestra,
		 * oltre la modalit� di chiusura
		 */
		frame = new JFrame("PoesieBPF");
		frame.setSize(frameWidth, frameHeight);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		
		/*
		 *  Creo un contenitore e lo aggiungo al contenitore
		 *  generale
		 */
		//JPanel cont = new JPanel(new BorderLayout());
		JPanel cont = new BackgroundedFrame(new File("resources/imgs/center.jpg"));
		cont.setLayout(new BorderLayout());
		frame.getContentPane().add(cont);
		
		
		/*
		 *  Creo il contenitore per il TITOLO
		 *  e lo aggiungo al contenitore "cont"
		 */
		//File fileImgTit = new File("resources/imgs/top.jpg");
		//jpTitle = new BackgroundedFrame(fileImgTit, jpTitle);
		jpTitle = new JPanel();
		jpTitle.setOpaque(false);
		cont.add(jpTitle, BorderLayout.NORTH);
		
		
		
		
		/*
		 * Creo il contenitore per i TITOLI delle poesie
		 * e lo aggiungo al contenitore "cont"
		 */
		jpTitles = new JPanel();
		jpTitles.setLayout(new BorderLayout());
		jpTitles.setOpaque(false);
		cont.add(jpTitles, BorderLayout.WEST);
		
		
		
		//File fileImgConts = new File("resources/imgs/center.jpg");
		//jpContents = new BackgroundedFrame(fileImgConts, jpContents);
		jpContents = new JPanel();
		jpContents.setOpaque(false);
		cont.add(jpContents, BorderLayout.CENTER);
		//jpContents = new JPanel();
		jpContents.setSize(10, 10);
		jpContents.setLayout(new GridLayout(1, 1));
		//jpContents.setBackground(Color.YELLOW);
		cont.add(jpContents, BorderLayout.CENTER);
		
		
		
		
		// i componenti generali
		CreaMenu();
		CreaTitolo();
		CreaPrincipale();
		CreaListaTitoli();
		
		
		
		//Display the window.
		frame.setVisible(true);
	}
	
	
	public void CreaMenu() {
		// Creazione la barra men�
		JMenuBar menubar = new JMenuBar();
		JMenu file = new JMenu("File");;
		ImageIcon fileCloseIco = new ImageIcon(
				"resources/imgs/exit.png");
				//PoesieBPF.class.getResource("imgs/exit.png"));
		JMenuItem fileClose = new JMenuItem("Close", fileCloseIco);
		fileClose.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent event) { System.exit(0); } });
		file.add(fileClose);
		JMenu about = new JMenu("Help");
		ImageIcon helpAboutIco = new ImageIcon(
				"resources/imgs/info.png");
				//PoesieBPF.class.getResource("imgs/info.png"));
		JMenuItem helpAbout = new JMenuItem("About", helpAboutIco);
		helpAbout.addActionListener(new AboutActionListener());
		about.add(helpAbout);
		menubar.add(file);
		menubar.add(about);
		frame.setJMenuBar(menubar);
	}
	
	// Azione del sottomen� about
	class AboutActionListener implements ActionListener{
		public void actionPerformed(ActionEvent event){
			JOptionPane.showMessageDialog(frame, "PoesieBPF\n" + 
					"by BERTOLOTTI Paolo Francesco\n\n" +
					"programma v. 2.0.0\n" +
					"database v. 0.5.0\n" +
					"      aggiornato al 17/04/2010");
		}
	}
	
	
	private void CreaTitolo() {
		/*
		 * Creo e setto l'elemento titolo (JTextArea)
		 * contenuto da contTitolo (JPanel)
		 * con layout (BorderLayout.CENTER)
		 */
		JTextArea titolo = new JTextArea();
		titolo.setFont(serif20Bold);
		titolo.setOpaque(false);
		titolo.setForeground(Color.RED);
		titolo.setText("Poesie dal cuore di\n\tBERTOLOTTI Paolo Francesco");
		titolo.setEditable(false);
		jpTitle.add(titolo);
		
	}
	
	
	
	
	
	/* 
	 * Creo l'oggetto della lista delle poesie listaPoesie (JList) 
	 * impostandolo come una sola colonna con una sola selezione possibile
	 * contenuto da contListaPoesie (JPanel)
	 * @throws java.lang.Exception a causa della classe usata da lxml
	 */
	private void CreaListaTitoli() throws Exception {
		listaPoesie = new JList();
		
		model = new DefaultListModel();
		
		listaPoesie.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
		listaPoesie.setLayoutOrientation(JList.VERTICAL);
		listaPoesie.setVisibleRowCount(5);
		LoadXMLPoesie.parseStream("RTIT");
		
		JScrollPane jspListaPoesie = new JScrollPane(listaPoesie);
		jspListaPoesie.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		jspListaPoesie.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		
		jpTitles.add(jspListaPoesie);
		
		ListSelectionListener selectionListener = new SelectionListener();
		
		
		
		listaPoesie.setOpaque(false);
		jspListaPoesie.setOpaque(false);
		jspListaPoesie.setOpaque(false);
		jspListaPoesie.getViewport().setOpaque(false);
		listaPoesie.setBackground(new Color(255,255,255,100));
		listaPoesie.setOpaque(false);
		
		
		
		
		
		listaPoesie.addListSelectionListener(selectionListener);
    }
    
	/*
	 * Classe per l'azione al cambiamento dell'oggetto selezionato quando cambia il titolo selezionato
	 */
	protected class SelectionListener implements ListSelectionListener {
		public void valueChanged(ListSelectionEvent e) {
    		
			try {
				Suoni.ol.close();
			} catch (Exception eeee) {
			
			}
			
			
			//System.out.println("OK");
			if(!e.getValueIsAdjusting()) {
				JList list = (JList)e.getSource();
				//System.out.println("");
				Object selectedItem = list.getSelectedValue();
				//System.out.println(selectedItem.toString()+"\n");
				LoadXMLPoesie.parseStream("RTPT", selectedItem.toString());
			} else {
				//System.out.println("NO! NO! NO!");
			}
		}
	} 
	
	
	
	
	
	/*
	 * Creazione dei componenti del JPanel jpContents
	 */
	protected void CreaPrincipale() {
		
		Font myFont;
		myFont = new Font("Arial", Font.BOLD, 16);
		
		tit = new JLabel("");
		tit.setForeground(Color.BLUE);
		tit.setFont(myFont);
		
		
		myFont = new Font("Arial", Font.PLAIN, 14);
		
		testoPoesia = new JTextArea(jpContents.getHeight(),jpContents.getWidth());
		testoPoesia.setEditable(false);
		testoPoesia.setLineWrap(true);
		testoPoesia.setWrapStyleWord(true);
		testoPoesia.setFont(myFont);
		
		//Contenitore ass per titolo e testo poesia
		JPanel ass = new JPanel(new BorderLayout());
		ass.add(tit, BorderLayout.NORTH);
		ass.add(testoPoesia, BorderLayout.CENTER);
		
		String presentazione = "Grazie per aver voluto vedere il mio programma. Spero che le mie poesie vi " +
				"piacciano.\n" +
				"Per commenti, segnalazioni, se siete interessati al codice sorgente o altro, potete contattarmi all'indirizzo email " +
				"bertolottipf@gmail.com segnalando nell'oggetto il programma in questione.\n\n" +
				"Programma, testi sotto licenza Common Creative.";
		
		tit.setText("Presentazione");
		testoPoesia.setText(presentazione);
		
		
		
		
		createActions();
		popup = new JPopupMenu();
		//popup.add(cutAction);
		popup.add(copyAction);
		//popup.add(pasteAction);
		
		
		// Aggiunge un MouseListener al componente che deve mostrare il menu
		MouseListener popupListener1 = new PopupListener();
		testoPoesia.addMouseListener(popupListener1);
		
		
		scrollbar = new JScrollPane(testoPoesia);
		scrollbar.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		scrollbar.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		
		
		
		//Trasparenze
		ass.setOpaque(false);
		tit.setOpaque(false);
		testoPoesia.setOpaque(false);
		scrollbar.setOpaque(false);
		scrollbar.setOpaque(false);
		scrollbar.getViewport().setOpaque(false);
		testoPoesia.setBackground(new Color(255,255,255,100));

		
		ass.add(tit, BorderLayout.NORTH);
		ass.add(scrollbar, BorderLayout.CENTER);
		
		jpContents.add(ass);
		
		
		
	}
	
	
	
	// Azione tasto destro del mouse sulla JTextArea testoPoesia
	protected class PopupListener extends MouseAdapter {
		public void mousePressed(MouseEvent e) {
			if (e.isPopupTrigger()) {
				popup.show(e.getComponent(),e.getX(), e.getY());
			}
		}
		public void mouseReleased(MouseEvent e) {
			if (e.isPopupTrigger()) {
				popup.show(e.getComponent(),e.getX(), e.getY());
			}
		}
	}
	
	// Azioni del men� popup sulla JTextArea testoPoesia
	@SuppressWarnings("serial")
	protected void createActions() {
		/*cutAction = new AbstractAction("taglia") {
			public void actionPerformed(ActionEvent e) {
				testoPoesia.cut();
			}
		};*/
		copyAction = new AbstractAction("copia") {
			public void actionPerformed(ActionEvent e) {
				testoPoesia.copy();
			}
		};
		/*pasteAction = new AbstractAction("incolla") {
			public void actionPerformed(ActionEvent e) {
				testoPoesia.paste();
			}
		};*/
	}
	
	
	
	
	public static void main(String[] args) throws Throwable {
		@SuppressWarnings("unused")
		PoesieBPF prog = new PoesieBPF();
	}
	
	
}




// http://forum.html.it/forum/showthread/t-1087331.html
class BackgroundedFrame extends JPanel
{
	BufferedImage img;
	int width;
	int height;
	
	
	/**
	 * Costruttore della classe  BackgroundedFrame
	 * 
	 * @param f => il file immagine che deve essere sfondo
	 * @param jpanel => il jpanel di cui f deve essere sfondo
	 */
	public BackgroundedFrame(File f) {
		super(true); //crea un JPanel con doubleBuffered true
		try {
			setImage(ImageIO.read(f));
		} catch(Exception e) {}
	}
	
	
	public void setImage(BufferedImage img) {
		this.img = img;
		width = img.getWidth();
		height = img.getHeight();
	}
	
	// sovrascrivi il metodo paintComponent passandogli l'immagine partendo dalle coordinate 0,0 senza usare un ImageObserver (null)
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawImage(img, 0, 0, null);
	}
}



